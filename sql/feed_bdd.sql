-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 10-05-2022 a las 23:13:11
-- Versión del servidor: 10.4.21-MariaDB
-- Versión de PHP: 8.0.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `feed_bdd`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auth_table`
--

CREATE TABLE `auth_table` (
  `id` int(11) NOT NULL COMMENT 'id de la tabla',
  `init_index` text COLLATE utf8mb4_spanish2_ci NOT NULL COMMENT 'init index',
  `nom` text COLLATE utf8mb4_spanish2_ci NOT NULL COMMENT 'usuario de login',
  `pas` text COLLATE utf8mb4_spanish2_ci NOT NULL COMMENT 'password de usuario',
  `id_usr` text COLLATE utf8mb4_spanish2_ci NOT NULL COMMENT 'id del usuario'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

--
-- Volcado de datos para la tabla `auth_table`
--

INSERT INTO `auth_table` (`id`, `init_index`, `nom`, `pas`, `id_usr`) VALUES
(5, 'tSCnwkhD', 'Beto', '$2y$10$sFwV6hDmb5DqaoZgy6q5JOSZK7edeitFIhpqVqcqrWz1qgfXZm8pS', 'g2Xk6Ku7'),
(8, 'IF63DMr4', 'testito', '$2y$10$0IRz11TjD/BzG71Xj9hkSeSDhhKP7P3fSOcWI7bCYdX5qlat2N4j2', 'JnM5Bjex');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `order_table`
--

CREATE TABLE `order_table` (
  `id` int(11) NOT NULL COMMENT 'id de la table',
  `id_per` text COLLATE utf8mb4_spanish2_ci NOT NULL COMMENT 'id del perfil',
  `att` text COLLATE utf8mb4_spanish2_ci NOT NULL COMMENT 'id de los attachments',
  `sch` datetime NOT NULL COMMENT 'calendarizacion de la order',
  `mai` text COLLATE utf8mb4_spanish2_ci NOT NULL COMMENT 'email de receptor',
  `fla` tinyint(1) NOT NULL COMMENT 'bandera de periodicidad'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `perf_table`
--

CREATE TABLE `perf_table` (
  `id` int(11) NOT NULL COMMENT 'id de la table',
  `id_usr` text COLLATE utf8mb4_spanish2_ci NOT NULL COMMENT 'id del perfil',
  `type` int(11) NOT NULL COMMENT 'Tipo de user',
  `nom` text COLLATE utf8mb4_spanish2_ci NOT NULL,
  `ape` text COLLATE utf8mb4_spanish2_ci NOT NULL,
  `apm` text COLLATE utf8mb4_spanish2_ci NOT NULL,
  `tel` text COLLATE utf8mb4_spanish2_ci NOT NULL,
  `cel` text COLLATE utf8mb4_spanish2_ci NOT NULL,
  `mai` text COLLATE utf8mb4_spanish2_ci NOT NULL COMMENT 'mail del usr',
  `fec` datetime NOT NULL COMMENT 'fecha de creacion de perfil',
  `id_per` text COLLATE utf8mb4_spanish2_ci NOT NULL COMMENT 'id del perfil'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

--
-- Volcado de datos para la tabla `perf_table`
--

INSERT INTO `perf_table` (`id`, `id_usr`, `type`, `nom`, `ape`, `apm`, `tel`, `cel`, `mai`, `fec`, `id_per`) VALUES
(5, 'g2Xk6Ku7', 1, 'Beto', '', '', '', '', 'm@il.com', '0000-00-00 00:00:00', 'SbbLn1JN'),
(6, 'JnM5Bjex', 1, 'testito', '__', '__', '0000000000', '0000000000', 'tes@t.com', '2022-03-30 13:02:50', 'Ne04cxS0');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `auth_table`
--
ALTER TABLE `auth_table`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `order_table`
--
ALTER TABLE `order_table`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `perf_table`
--
ALTER TABLE `perf_table`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `auth_table`
--
ALTER TABLE `auth_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id de la tabla', AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `order_table`
--
ALTER TABLE `order_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id de la table';

--
-- AUTO_INCREMENT de la tabla `perf_table`
--
ALTER TABLE `perf_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id de la table', AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
