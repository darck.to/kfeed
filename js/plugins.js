// Avoid `console` errors in browsers that lack a console.
(function() {
  var method;
  var noop = function () {};
  var methods = [
    'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
    'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
    'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
    'timeline', 'timelineEnd', 'timeStamp', 'trace', 'warn'
  ];
  var length = methods.length;
  var console = (window.console = window.console || {});

  while (length--) {
    method = methods[length];

    // Only stub undefined methods.
    if (!console[method]) {
      console[method] = noop;
    }
  }
}());

// Place any jQuery/helper plugins in here.
(function() {
  //CREA UN TOAST MODAL
  window.toast = function(content) {
    if (!$('#toast').length) {
      $('body').append('<div id="toast"></div>');
    }
    var toast = '<span class="toast-tag">' + content + '</span>';
    $('#toast').append(toast);
    setTimeout(function(){
      if ($('.toast-tag').length > 0) {
        $('.toast-tag').fadeOut('fast', function() {
          $(this).remove()
        })
      }
    }, 4000)
  }
  //CREA UN MODAL PARA TODOS Y TODO
  window.modal = function(incoming) {
    //DESTRUYE VIEJOS MODALES
    if ($('.modal').length) {
      $('.modal').fadeOut('fast', function() {
        $(this).remove()
      })
    }
    //CREA UN MODAL NUEVO
    var content = incoming;
    var modal = '<div class="modal">';
    modal += '<div class="modal-background"></div>';
    modal += '<div class="modal-content">';
      modal += '<div class="box has-bg-white">';
        modal += content;
      modal += '</div>';
    modal += '</div>';
    modal += '<div class="modal-absolute"></div>';
    modal += '<button class="modal-close is-large" aria-label="close"></button>';
    modal += '</div>';
    $('body').prepend(modal);
    $('.modal').css('z-index','9000');
    $('.modal').toggleClass("is-active");
    $('.modal-close').on('click', function(){
      $('.modal').fadeOut('fast', function() {
        $(this).remove()
      })
    });
    $('.modal-background').on('click', function(){
      $('.modal').fadeOut('fast', function() {
        $(this).remove()
      })
    })
  }
  //MODAL SOLO CLAUSURABLE CON BOTON
  window.modap = function(incoming) {
    //DESTRUYE VIEJOS MODALES
    if ($('.modal').length) {
      $('.modal').fadeOut('fast', function() {
        $(this).remove()
      })
    }
    //CREA UN MODAL NUEVO
    var content = incoming;
    var modal = '<div class="modal">';
    modal += '<div class="modal-background"></div>';
      modal += '<div class="modal-content">';
        modal += '<div class="box has-bg-white">';
          modal += content;
        modal += '</div>';
      modal += '</div>';
      modal += '<div class="modal-absolute"></div>';
      modal += '<button class="modal-close is-large" aria-label="close"></button>';
    modal += '</div>';
    $('body').prepend(modal);
    $('.modal').css('z-index','9000');
    $('.modal').toggleClass("is-active");
    $('.modal-close').on('click', function() {
      $('.modal').fadeOut('fast', function() {
        $(this).remove()
      })
    })
  }
  //CIERRAMODAL
  window.modax = function(e) {
    //DESTRUYE VIEJOS MODALES
    if ($('.modal').length) {
      $('.modal').fadeOut('fast', function() {
        $(this).remove()
      })
    }
  }
}());