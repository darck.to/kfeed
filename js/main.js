(function() {
    // Menu
    $('#menu').load('templates/menu/menu.html', function(e) {
        // Ahora la Portada
    });
    // Define Datetime-local and inputs
    $('#scTimeToSend').val(new Date().toJSON().slice(0,19));
    $('#apvdEmailToSend').val('');
    $('#adEmailToSend').val('');
    $('#flToSend').val('');
    // On change Datetime-local
    $('#scTimeToSend').on('change', function(e){
        toast($('#scTimeToSend').val())
    })

    //Feed analyser
    $('#rssToSend').on('keypress', function(e) {
        if(e.which == 13) {
            // Tag var
            let tag = "";
            // String to search, could be rss or tiwtter feed to search
            let feedStr = $(this).val();
            // Check for http or not http
            let expression = /https?:\/\/?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)/gi;
            let regex = new RegExp(expression);

            if (feedStr.match(regex)) {
                toast("Semms Https");
                // Check for rss
                $("#rss-feeds").rss(feedStr, {
                    limit: 0,
                    entryTemplate: '<li><a href="{url}">[{author}@{date}] {title}</a><br/>{shortBodyPlain}</li>',
                    success: function() {
                        toast('Valid RSS');
                        // Add to feeds to send
                        tag += '<span class="tag has-bg-warning">' + feedStr + '</span>';
                        $('#feeds').append(tag)
                    },
                    error: function() {
                        toast('Invalid RSS')
                    }
                })
            } else {
                toast("No https");
                // Add to feeds to send
                tag += '<span class="tag has-bg-blue-light">' + feedStr + '</span>'
                // Add tags to feed div
                $('#feeds').append(tag)
            }
            // Delete tags
            $('.tag').on('click', function(e) {
                $(this).remove()
            })
        }
    });

    // Email validation
    $('#apvdEmailToSend').on('keyup', function(e) {
        if (!validateEmailInput($(this))) {
            $(this).css('border','1px solid red');
        } else {
            $(this).css('border','1px solid green');
        }
    })
    // Email validation
    $('#adEmailToSend').on('keyup', function(e) {
        if (!validateEmailInput($(this))) {
            $(this).css('border','1px solid red');
        } else {
            $(this).css('border','1px solid green');
        }
    })
    
    // Read inputs at send
    $('#btToSend').on('click', function(e) {
        let scTime = false;
        // 1 file per order
        let file = $("#flToSend")
        if (parseInt(file.get(0).files.length) > 1) {
            toast('Only 1 file per entry');
            $('#flToSend').css('border','1px solid red');
            return false
        }
        // If date time then schedule
        if ($('#scTimeToSend').val()) {
            toast('Delivery scheduled ' + $('#scTimeToSend').val());
            scTime = $('#scTimeToSend').val()
        }
        // Need approved kindle email
        if (!$('#apvdEmailToSend').val()) {
            toast('No approved mail');
            $('#apvdEmailToSend').css('border','1px solid red')
        }
        // Need remitent
        if (!$('#adEmailToSend').val()) {
            toast('No addressee mail');
            $('#adEmailToSend').css('border','1px solid red')
        }
        // Send to process
        sendToPhp(scTime)
    })
}());

// Email validation
function validateEmailInput(inputText) {
    var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (inputText.val().match(mailformat)) {
        toast("Valid email address!");
        return true
    } else {
        toast("Invalid email address!");
        return false
    }
}

// Function that sends the info to remitent
function sendToPhp(sc) {
    let scTime = sc;
    var formData = new FormData();
    
    formData.append("file", document.getElementById('flToSend').files[0]);
    // If sc
    if (scTime) {
        formData.append('time', $('#scTimeToSend').val());
    }
    formData.append('kemail', $('#apvdEmailToSend').val());
    formData.append('aemail', $('#adEmailToSend').val());
    
    var formMethod = 'Post';
    var rutaScrtip = 'php/send/send-file.php';

    var request = $.ajax({
        url: rutaScrtip,
        method: formMethod,
        data: formData,
        contentType: false,
        processData: false,
        async: true,
        dataType: 'json',
        crossDomain: true,
        context: document.body,
        cache: false,
    });
    // handle the responses
    request.done(function(data) {
        $.each(data, function (name, value) {
            toast(value.message)
        })
    })
    request.fail(function(jqXHR, textStatus) {
        console.log(textStatus);
    })
    request.always(function(data) {
        // Refresh New Entry!
        //$('#scTimeToSend').val(new Date().toJSON().slice(0,19));
        //$('#apvdEmailToSend').val('');
        //$('#adEmailToSend').val('');
        //$('#flToSend').val('');
    });
}