(function() {
    // Check for click events on the navbar burger icon
    $(".navbar-burger").click(function() {
        $(".navbar-burger").toggleClass("is-active");
        $(".navbar-menu").toggleClass("is-active")
    });
    // SI existe session mandamos el menú de usuario
    compruebaLogin(localStorage.getItem("feed_token"));
}());

function compruebaLogin(t) {
    var content = "";
    if (t == 0 || t == null) {
        content += '<a href="" class="navbar-item">';
            content += 'Planes';
        content += '</a>';
        content += '<a href="" class="navbar-item buttonSignup">';
            content += 'Crea tu Cuenta';
        content += '</a>';
        content += '<a href="" class="navbar-item buttonLogin">';
            content += 'Inicia Sesi&oacute;n';
        content += '</a>'
    } else {
        content += '<a href="" class="navbar-item" href="dashboard.html">';
            content += '<span class="icon"><i class="fa-solid fa-gauge"></i></span>';
            content += '<span>Dashboard</span>';
        content += '</a>';
        content += '<a href="" class="navbar-item buttonLogout">';
            content += 'Cierra Sesi&oacute;n';
        content += '</a>'
    }
    $('#menu-buttons').html(content)

    // Carga Login
    $('.buttonLogin').on('click', function(e) {
        e.preventDefault();
        $.get('templates/init/init-login.html', function (data) {
            modal(data);
        })
    })
    // Carga Sign up
    $('.buttonSignup').on('click', function(e) {
        e.preventDefault();
        $.get('templates/init/init-signin.html', function (data) {
            modal(data);
        });
        //$('.modal-content').load('templates/init/init-signin.html');
    })
    // Login out
    $('.buttonLogout').on('click', function(e) {
        e.preventDefault();
        localStorage.removeItem("feed_key");
        localStorage.removeItem("feed_user");
        localStorage.removeItem("feed_token");
        modal('<h1 class="has-text-centered">... cerrando sesi&oacute;n</h1>');
        setTimeout(location.reload.bind(location), 1000);
    })
}
   