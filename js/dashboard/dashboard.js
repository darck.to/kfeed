(function() {
    // Comprobamos si existe la session
    var stringafeed_key = localStorage.getItem("feed_key");
    var stringafeed_user = localStorage.getItem("feed_user");
    var loginToken = localStorage.getItem("feed_token");
    comprueba_login(stringafeed_key,stringafeed_user,loginToken);
}());


function comprueba_login(a,u,t) {
  //COMPROBAMOS SI EXISTE LOGIN LOCAL
  if (t == 0 || t == null) {
    //LOCAL LOGIN NULL OR NEGATIVE
    $.ajax({
      type: 'POST',
      url: 'php/init/init-comprueba-auth.php',
      data: {
        auth : a,
        user : u
      },
      async: true,
      dataType : 'json',
      crossDomain: true,
      context: document.body,
      cache: false,
      success: function(data) {
        var aprobacion
        $.each(data, function (name, value) {
          if (value.success == true) {
            localStorage.setItem("feed_token",1);
            localStorage.setItem("feed_type",value.type);
            toast(value.message)
            //LOGIN LOCAL POSITIVE
            cargaDashboard()
          } else {
            modap();
            $('.modal-close').remove();
            localStorage.setItem("feed_token",0);
            $('.modal-content').load('templates/init/init-login.html')
          }
        });
      },
      error: function(xhr, tst, err) {
        toast('El tiempo de espera fue superado, por favor intentalo en un momento mas');
        modax();
      }
    })
  } else {
    //LOGIN LOCAL POSITIVE
    cargaDashboard()
  }
}

function cargaDashboard(e) {
    var txt = '<div class="columns is-multiline is-gapless">';
        txt += '<div class="column is-2">';
            txt += '<div class="dashboard-menu"></div>';
        txt += '</div>';
        txt += '<div class="column is-10">';
            txt += '<div class="dashboard-body"></div>';
        txt += '</div>';
    txt += '</div>';
    $('.dashboard-main').html(txt)
    // carga Menu Dashboard
    $('.dashboard-menu').load('templates/dashboard/dashboard-menu.html', function (e) {
        $('.menu a').on('click', function (e) {
            $('.menu a').removeClass('is-active');
            $(this).toggleClass('is-active')

        })
        // Cargamos las secciones del menu en body
        $('.menu a').on('click', function (e) {
            e.preventDefault();
            $('.dashboard-body').html('Soy: ' + $(this).data('target'));
            $('.dashboard-body').load('templates/dashboard/dashboard-' + $(this).data('target') + '.html')
        })

        // Login out
        $('.buttonLogout').on('click', function(e) {
            localStorage.removeItem("feed_key");
            localStorage.removeItem("feed_user");
            localStorage.removeItem("feed_token");
            modal('<h1 class="has-text-centered">... cerrando sesi&oacute;n</h1>')
            setTimeout(location.reload.bind(location), 1000);
        })
    })
}