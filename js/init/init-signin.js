$(function() {

  $('focus',':input', function(){
    $(this).attr('autocomplete','off')
  })

  $('.buttonLoginForm').on('click', function(e){
    $.get('templates/init/init-login.html', function (data) {
      modal(data)
    })
  })

  //REGISTRAMOS EL USUARIO
  $('#initRegisterUser').submit(function(event) {
    event.preventDefault()
    // Mostramos los planes
    $.get('templates/init/init-plans.html', function (data) {
      $('.modal-content').append(data);
      return false
    })

    var formData = new FormData(document.getElementById("initRegisterUser"));
    var formMethod = $(this).attr('method');
    var rutaScrtip = $(this).attr('action');

    var request = $.ajax({
      url: rutaScrtip,
      method: formMethod,
      data: formData,
      contentType: false,
      processData: false,
      async: true,
      dataType : 'json',
      crossDomain: true,
      context: document.body,
      cache: false,
    });
    // handle the responses
    request.done(function(data) {
      $.each(data, function (name, value) {
        if (value.success) {
          $('.signButton').addClass('is-loading');
          localStorage.setItem("feed_key", value.feed_key);
          localStorage.setItem("feed_user", value.feed_user);
          localStorage.setItem("feed_token", 1);
          //INICIALIZA LA TIENDA
          carga_session()
        } else {
          toast(value.error)
        }
      })

    })
    request.fail(function(jqXHR, textStatus) {
      console.log(textStatus);
    })
    request.always(function(data) {
      // clear the form
      $('#initRegisterUser').trigger('reset');
    })

    //CARGA SESSION
    function carga_session(e) {
      $.ajax({
        type: 'POST',
        url: 'php/init/init-start.php',
        data: {
          a: 1
        },
        cache: false,
        success: function(data) {
          $.each(data, function (name, value) {
            if (value.success) {
              toast(value.message);
              window.location.href = '.'
            } else {
              toast(value.message)
            }
          })
        },
        error: function(xhr, tst, err) {
          console.log(err);
        }
      })
    }

  })

  //EMAIL
  $('#r-mai').on('keyup', function() {
    email = $(this).val();
    if (validateEmail(email)) {
      $('.signButton').prop('disabled',false);
    } else {
      $('.signButton').prop('disabled',true);
    }

    if (this.value.length == 0) {
      $('.signButton').prop('disabled',true);
    }
  });

  //CONFIMATION
  $('#r-pac').on('keyup', function() {
    pass = $('#r-pas').val();
    confirm = $(this).val();
    if (confirm != pass) {
      $('.signButton').prop('disabled',true);
    } else {
      $('.signButton').prop('disabled',false);
    }

    if (this.value.length == 0) {
      $('.signButton').prop('disabled',true);
    }
  })

  function validateEmail(sEmail) {
    var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    if (filter.test(sEmail)) {
      return true;
    }
    else {
      return false;
    }
  }

});
