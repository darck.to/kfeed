$(function() {

  $('focus',':input', function(){
    $(this).attr('autocomplete','off');
  })

  $('.buttonRegisterForm').on('click', function(e){
    $.get('templates/init/init-signin.html', function (data) {
      modal(data);
    });
  })

  //LOGIN DEL USUARIO
  $('#initLoginUser').submit(function(event) {
    event.preventDefault();

    var formData = new FormData(document.getElementById("initLoginUser"));
    var formMethod = $(this).attr('method');
    var rutaScrtip = $(this).attr('action');

    var request = $.ajax({
      url: rutaScrtip,
      method: formMethod,
      data: formData,
      contentType: false,
      processData: false,
      async: true,
      dataType: 'json',
      crossDomain: true,
      context: document.body,
      cache: false,
    });
    // handle the responses
    request.done(function(data) {
      var aprobacion, key, user;
      $.each(data, function (name, value) {
        aprobacion = value.success;
        key = value.feed_key;
        user = value.feed_user;
      });
      if (aprobacion == true) {
        $('.logButton').addClass('is-loading');
        localStorage.setItem("feed_key", key);
        localStorage.setItem("feed_user", user);
        localStorage.setItem("feed_token", 1);
        carga_session()
      } else {
        toast('Usuario o contraseña equivocada')
      };
    })
    request.fail(function(jqXHR, textStatus) {
      console.log(textStatus);
    })
    request.always(function(data) {
      // clear the form
      $('#initLoginUser').trigger('reset')
    });

    function carga_session(e) {
      $.ajax({
        type: 'POST',
        url: 'php/init/init-start.php',
        data: {
          a: 1
        },
        cache: false,
        success: function(data) {
          $.each(data, function (name, value) {
            if (value.success) {
              toast(value.message);
              window.location.href = '.'
            } else {
              toast(value.message)
            }
          })
        },
        error: function(xhr, tst, err) {
          console.log(err)
        }
      })
    }

  })

  $('.buttonRecoverPass').on('click', function(e) {
    var content = '<div class="box">';
      content += '<div class="has-text-centered">';
        content += '<h1 class="has-text-centered has-text-danger is-size-4">Introduce tu correo para recuperar tu contraseña</h1>';
        content += '<input id="i-nom" type="text" class="input" placeholder="Nombre de usuario">';
        content += '<h1 class="has-text-right"><span class="is-size-7 has-text-grey ma-one pa-one">Enviaremos un nuevo password a tu buzon de correo</span></h1>';
        content += '<button class="button is-primary ma-one recoverPassword">Enviar</button>';
      content += '</div>';
      content += '<button class="button is-dark is-small buttonReturnrForm">Regresar</button>';
    content += '</div>';
    modap(content)

    $('.recoverPassword').on('click', function(e){
      var nom = $('#i-nom').val();
      $.ajax({
        type: 'POST',
        url: 'php/init/init-recover-password.php',
        data: {
          nom: nom
        },
        async:true,
        dataType : 'json',
        crossDomain: true,
        context: document.body,
        cache: false,
        success: function(data) {
          $.each(data, function(name, content){
            if(content.success == true) {
              toast(content.message)
            } else {
              toast(content.message)
            }
          })
        },
        error: function(xhr, tst, err) {
          toast(data)
        },
        timeout: 10000
      })
    })

    //REGRESA A LOGIN
    $('.buttonReturnrForm').on('click', function(e){
      $('.modal-content').load('templates/init/init-login.html');
    })
  })

})