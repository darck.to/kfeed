<?php
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
    header("Access-Control-Allow-Origin: *");
    header('Content-type: application/json');
    include_once('../functions/abre_conexion.php');
    include_once('../functions/functions.php');

    require('../functions/fpdf/fpdf.php');

    date_default_timezone_set("America/Mexico_City");
    $fechaActual = Date('Y-m-d H:i:s');
    $localIP = getHostByName(getHostName());

    $random = generateRandomString(9);
    $resultados = array();
    
    // "limpiamos" los campos del formulario de posibles códigos maliciosos
    $time = mysqli_real_escape_string($mysqli, $_POST['time']);
    $kemail = mysqli_real_escape_string($mysqli, $_POST['kemail']);
    $aemail = mysqli_real_escape_string($mysqli, $_POST['aemail']);
    // Dir
    $uploaddir = "../../assets/files/";
    $uploadfile = $uploaddir . "pdf_" . $random . ".pdf";
    if (move_uploaded_file($_FILES['file']['tmp_name'], $uploadfile)) {
        $resultados[] = array("success"=> true, "ip"=> $localIP, "date"=> $fechaActual, "message"=> "Archivos Subidos en " . $uploadfile);
    } else {
        $resultados[] = array("success"=> false, "ip"=> $localIP, "date"=> $fechaActual, "message"=> "No se subio el archivo, contacta soporte");
    }
    
    print json_encode($resultados);
    // incluimos el archivo de desconexion a la Base de Datos
    include('../functions/cierra_conexion.php');
?>