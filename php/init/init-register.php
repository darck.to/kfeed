<?php
  header("Access-Control-Allow-Origin: *");
  header('Content-type: application/json');
  include_once('../functions/abre_conexion.php');
  include_once('../functions/functions.php');

  date_default_timezone_set("America/Mexico_City");
  $fechaActual = Date('Y-m-d H:i:s');
  $localIP = getHostByName(getHostName());

  $resultados = array();

  // "limpiamos" los campos del formulario de posibles códigos maliciosos
  $usuario_nombre = mysqli_real_escape_string($mysqli, $_POST['nom']);
  $usuario_email = mysqli_real_escape_string($mysqli, $_POST['mai']);
  $usuario_clave = mysqli_real_escape_string($mysqli, $_POST['pas']);

  $init_index = generateRandomString(8);
  $id_usr = generateRandomString(8);
  $id_per = generateRandomString(8);

  // comprobamos que el usuario ingresado no haya sido registrado antes
  $sql = $mysqli->query("SELECT nom FROM auth_table WHERE nom ='".$usuario_nombre."'");
  if ($sql->num_rows > 0) {
    $resultados[] = array("success"=> false, "type"=> "register", "ip"=> $localIP, "date"=> $fechaActual, "error"=> "El usuario ya ha sido registrado previamente");
  } else {
    $sql = $mysqli->query("SELECT mai FROM perf_table WHERE mai ='".$usuario_email."'");
    if ($sql->num_rows > 0) {
      $resultados[] = array("success"=> false, "type"=> "register", "ip"=> $localIP, "date"=> $fechaActual, "error"=> "El correo ya ha sido registrado previamente");
    } else {
      $usuario_clave = password_hash($usuario_clave, PASSWORD_BCRYPT); // encriptamos la contraseña ingresada con md5
      $sqlReg = $mysqli->query("INSERT INTO auth_table (nom, pas, init_index, id_usr) VALUES ('".$usuario_nombre."', '".$usuario_clave."', '".$init_index."', '".$id_usr."')");
      if($sqlReg) {
        //GUARDAMOS EL PERFIL
        if ($sqlPerf = $mysqli->query("INSERT INTO perf_table (id_usr, type, mai, nom, ape, apm, tel, cel, id_per, fec) VALUES ('".$id_usr."', 1, '".$usuario_email."', '".$usuario_nombre."', '__', '__', '0000000000', '0000000000', '".$id_per."', '".$fechaActual."')")) {
          $resultados[] = array("success"=> true, "type"=> "register", "ip"=> $localIP, "date"=> $fechaActual, "feed_key"=> $init_index, "feed_user"=> $usuario_nombre);
        } else {
          $resultados[] = array("success"=> false, "type"=> "register", "ip"=> $localIP, "date"=> $fechaActual, "error"=> "Error, contact support ".mysqli_error($mysqli));
          //$resultados[] = array("success"=> false, "error"=> mysqli_error($mysqli));
        }
      } else {
        $resultados[] = array("success"=> false, "type"=> "register", "ip"=> $localIP, "date"=> $fechaActual, "error"=> "Error, contact support");
        //$resultados[] = array("success"=> false, "error"=> mysqli_error($mysqli));
      }
    }
  }

  print json_encode($resultados);
  // incluimos el archivo de desconexion a la Base de Datos
  include('../functions/cierra_conexion.php');
?>
